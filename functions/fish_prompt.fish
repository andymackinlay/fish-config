function fish_prompt --description 'Write out the prompt'
    set -l last_status $status


    set -l color_cwd
    set -l suffix
    switch $USER
        case root toor
            if set -q fish_color_cwd_root
                set color_cwd $fish_color_cwd_root
            else
                set color_cwd $fish_color_cwd
            end
            set suffix '#'
        case '*'
            set color_cwd $fish_color_cwd
            set suffix '»'
    end

    # Print a red dot for failed commands.
    if test $last_status -gt 0
      set_color red;
#      echo -n "⨉"
      echo -n " "
      set_color normal;
    else
      echo -n " "
    end

    if set -q VIRTUAL_ENV
      echo -n -s (set_color magenta) "(" (basename "$VIRTUAL_ENV") ")" (set_color normal) " "
    end

    echo -n -s (set_color $color_cwd) (prompt_pwd) (set_color normal) "$suffix "
end
