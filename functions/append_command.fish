function append_command --description "append arg to current or last command"
  set -l append $argv[1]
  if test -z "$append"
    echo "append_command needs one argument."
    return 1
  end

  set -l cmd (commandline)
  if test -z "$cmd"
    commandline -r $history[1]
  end

  set -l old_cursor (commandline -C)
  commandline -i " $append"
end
