function fish_right_prompt -d "Write out the right prompt"
# Just calculate this once, to save a few cycles when displaying the prompt
  if not set -q __fish_prompt_hostname
      set -g __fish_prompt_hostname (hostname|cut -d . -f 1)
  end

  set_color 666;
  echo -n -s '«' "$USER" @ "$__fish_prompt_hostname"
	set_color normal
end
