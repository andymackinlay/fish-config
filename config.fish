ssh-add -A ^ /dev/null

function fish_user_key_bindings
  bind \cq kill-word
  bind \cs 'prepend_command sudo'
  bind \cP 'append_command "| pbcopy"'
end

eval (python3 -m virtualfish)

status --is-interactive; and source (pyenv init -|psub)

test -e {$HOME}/.iterm2_shell_integration.fish ; and source {$HOME}/.iterm2_shell_integration.fish

function s
    ssh $argv
end

complete -c s --wraps ssh

function cu
    for i in (seq $argv[1])
        cd ..
    end
end
    
function empty
    for f in $argv
        cat /dev/null > $f
    end
end